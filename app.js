var app = require('http').createServer(handler)
, io = require('socket.io').listen(app)
, fs = require('fs'), net = require('net')

app.listen(8080);

function handler (req, res) {
	fs.readFile(__dirname + '/index.html',
				function (err, data) {
					if (err) {
						res.writeHead(500);
						return res.end('Error loading index.html');
					}

					res.writeHead(200);
					res.end(data);
				});
}

var websockets = [];

io.sockets.on('connection', function (websocket) {
	websockets.push( websocket );
	websocket.emit('news', { hello: 'world' });
	websocket.on('end', function () {
		var i = websockets.indexOf(socket)
		websockets.splice(i, 1)
	});
});

var server = net.Server( function(socket) {
	socket.on('data', function(d) {
		for ( var i = 0; i < websockets.length; i++ ) {
			websockets[i].emit('news', { hello: d });
		}
	})
}).listen(8000)


