var express = require('express')
var net = require('net');
var child = require('child_process');

var cmd = 'gst-launch';
var args = '';
var options = null;
  // {env: {LD_LIBRARY_PATH: '/usr/local/lib',
  // PATH: '/usr/local/bin:/usr/bin:/bin'}};


var app = express.createServer();

app.get('/', function(req, res) {
	var date = new Date();

	res.writeHead(200, {
		'Date':date.toUTCString(),
		'Connection':'close',
		'Cache-Control':'private',
		'Content-Type':'video/webm',
		'Server':'CustomStreamer/0.0.1',
    });

	var server = net.createServer(function (socket) {
		socket.on('data', function (data) {
			res.write(data);
		});
		socket.on('close', function(had_error) {
			res.end();
		});
	});

	server.listen();

	console.log( server.address().port )



});

app.listen(9001);

function onSpawnError(data) {
	console.log(data.toString());
}

function onSpawnExit(code) {
	if (code != null) {
		console.error('GStreamer error, exit code ' + code);
	}
}

process.on('uncaughtException', function(err) {
	console.debug(err);
});