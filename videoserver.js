// too much memory consumption
// need to save and segment the incoming data
// support streaming mid-stream, currently only works by launching the video latter

var net = require('net');

var sockets = [];

var s = net.Server(function(socket) {
	sockets.push(socket);
	socket.on('data', function(d) {
		for ( var i = 0; i < sockets.length; i++ ) {
			if ( sockets[i] == socket ) continue;
			sockets[i].write(d);
		}
	})

	socket.on('end', function() {
		var i = sockets.indexOf(socket)
		sockets.splice(i, 1)
	})

}).listen(8000)

process.on('uncaughtException', function(err) {
	console.debug(err);
});

// run this commmand to stream the video:
// gst-launch-0.10 v4l2src ! ffmpegcolorspace ! vp8enc speed=2 ! queue2 ! m. autoaudiosrc ! audioconvert ! vorbisenc ! queue2 ! m. webmmux name=m streamable=true ! tcpclientsink host=localhost port=8000