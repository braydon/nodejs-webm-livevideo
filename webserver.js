var http = require('http')
var net = require('net');

http.createServer( function( req, res ) {

	var date = new Date();

	res.writeHead(200, {
		'Date':date.toUTCString(),
		'Connection':'closed',
		'Cache-Control':'no-cache',
		'Content-Type':'video/webm',
		'Server':'CustomStreamer/0.0.1',
    });
	
	var client = net.createConnection(8000, function() {
		console.log('connected')
	})

	client.on('data', function(data) {
		res.write(data)
	});


}).listen(8080);

process.on('uncaughtException', function(err) {
	console.debug(err);
});